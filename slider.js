if ($('.js-slider').length > 0) {

  var height = '-' + $('.js-slider__section').height().toString() + 'px'

  $('.js-slider__nav__bullet#section-1').addClass('active');

  $('.js-slider__nav__bullet#section-1').on('click', function () {
    $('.js-slider__section').css('margin-top', '0')
    $('.js-slider__nav__bullet').removeClass('active')
    $('.js-slider__nav__bullet#section-1').addClass('active');
  })

  $('.js-slider__nav__bullet#section-2').on('click', function () {
    $('.js-slider__section').css('margin-top', '0')
    $('.js-slider__section#1').css('margin-top', height)
    $('.js-slider__nav__bullet').removeClass('active')
    $('.js-slider__nav__bullet#section-2').addClass('active');
  })

  $('.js-slider__nav__bullet#section-3').on('click', function () {
    $('.js-slider__section').css('margin-top', '0')
    $('.js-slider__section#1').css('margin-top', height)
    $('.js-slider__section#2').css('margin-top', height)
    $('.js-slider__nav__bullet').removeClass('active')
    $('.js-slider__nav__bullet#section-3').addClass('active');
  })

  $('.js-slider__nav__bullet#section-4').on('click', function () {
    $('.js-slider__section').css('margin-top', height)
    $('.js-slider__section#4').css('margin-top', '0')
    $('.js-slider__nav__bullet').removeClass('active')
    $('.js-slider__nav__bullet#section-4').addClass('active');
  })

  if ($('.js-slider__nav').is(':visible')) {
    var bullets = $('.js-slider__nav__bullet')

    var i = 0;
    var autoClick = setInterval(function() {
      var bullet = bullets[i++];
      bullet.click()
      if(i >= bullets.length) i = 0;
    }, 5000);

    $('.js-slider__nav').on('mouseover', function() {
      clearInterval(autoClick);
    })
  }
}